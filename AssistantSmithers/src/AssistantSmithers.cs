﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistantSmithers
{    
    static class AssistantSmithers
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }

    static class Settings
    {
        private static Properties.Settings settings = Properties.Settings.Default;

        private static bool DarkMode;

        private static StringCollection RecentFiles;
        private static readonly int MaxRecentFileHistory = 10;

        public static void Load()
        {
            DarkMode = settings.DarkMode;
            RecentFiles = settings.RecentFiles;

            if (RecentFiles == null)
            {
                InitialiseRecentFiles();
            }
        }

        public static void Save()
        {
            settings.DarkMode = DarkMode;
            settings.RecentFiles = RecentFiles;

            settings.Save();
        }

        private static void InitialiseRecentFiles()
        {
            RecentFiles = new StringCollection();

            for (int i = 0; i < 5; i++)
            {
                RecentFiles.Add("<blank>");
            }

            Save();
        }

        private static void TrimRecentFiles()
        {
            for (int i = MaxRecentFileHistory; i < RecentFiles.Count; i++)
            {
                RecentFiles.RemoveAt(i);
            }
        }

        public static void SetDarkMode(bool darkModeState)
        {
            Load();
            DarkMode = darkModeState;
            Save();
        }

        public static bool GetDarkMode()
        {
            Load();
            return DarkMode;
        }

        public static string GetRecentFile(int index)
        {
            Load();
            
            if (index < RecentFiles.Count)
            {
                return RecentFiles[index];
            }
            else
            {
                return "";
            }
        }

        public static StringCollection GetRecentFiles()
        {
            Load();
            return RecentFiles;
        }

        public static void RemoveRecentFile(string file)
        {
            Load();
            RecentFiles.Remove(file);
            Save();
        }

        public static void UpdateTopRecentFile(string file)
        {
            Load();
            RecentFiles.Remove(file);
            RecentFiles.Insert(0, file);
            TrimRecentFiles();
            Save();
        }
    }

    static class World
    {
        public static string Name;
        public static string File;
        public static List<Recipe> Recipes = new List<Recipe>();

        public static void Reset()
        {
            Name = null;
            File = null;
            Recipes = new List<Recipe>();
        }

        public static Metal GetCurrentAgeMetal()
        {
            Dictionary<Metal, int> dic = new Dictionary<Metal, int>();
            int highestTier = 0;

            foreach (Recipe recipe in Recipes)
            {
                if (recipe.Material.Tier >= highestTier)
                {
                    if (!dic.ContainsKey(recipe.Material)) dic.Add(recipe.Material, 0);

                    highestTier = recipe.Material.Tier;
                    dic[recipe.Material] += 1;
                }
            }

            return dic.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
        }
    }
}
