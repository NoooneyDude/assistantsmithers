﻿namespace AssistantSmithers
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabelWorldName = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSaveState = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelWelcome = new System.Windows.Forms.Panel();
            this.labelRecent5 = new System.Windows.Forms.Label();
            this.labelRecent4 = new System.Windows.Forms.Label();
            this.labelRecent3 = new System.Windows.Forms.Label();
            this.labelRecent2 = new System.Windows.Forms.Label();
            this.labelRecent1 = new System.Windows.Forms.Label();
            this.linkLabelRecent5 = new System.Windows.Forms.LinkLabel();
            this.linkLabelRecent4 = new System.Windows.Forms.LinkLabel();
            this.linkLabelRecent3 = new System.Windows.Forms.LinkLabel();
            this.linkLabelRecent2 = new System.Windows.Forms.LinkLabel();
            this.linkLabelRecent1 = new System.Windows.Forms.LinkLabel();
            this.labelRecent = new System.Windows.Forms.Label();
            this.linkLabelOpenWorld = new System.Windows.Forms.LinkLabel();
            this.linkLabelNewWorld = new System.Windows.Forms.LinkLabel();
            this.labelStart = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.contextMenuStripRecipePanel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemViewRecipeNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemEditRecipe = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDeleteRecipe = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxFilterRecipes = new System.Windows.Forms.GroupBox();
            this.labelFilterMaterial = new System.Windows.Forms.Label();
            this.labelSearch = new System.Windows.Forms.Label();
            this.comboBoxFilterMaterial = new System.Windows.Forms.ComboBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageIngots = new System.Windows.Forms.TabPage();
            this.recipeIngotsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.recipeGeneralContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageTools = new System.Windows.Forms.TabPage();
            this.recipeToolsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageWeapons = new System.Windows.Forms.TabPage();
            this.recipeWeaponsContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageArmour = new System.Windows.Forms.TabPage();
            this.recipeArmourContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPageBlocks = new System.Windows.Forms.TabPage();
            this.recipeBlocksContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonDeleteRecipe = new System.Windows.Forms.Button();
            this.buttonEditRecipe = new System.Windows.Forms.Button();
            this.buttonAddRecipe = new System.Windows.Forms.Button();
            this.panelRecipes = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panelWelcome.SuspendLayout();
            this.contextMenuStripRecipePanel.SuspendLayout();
            this.groupBoxFilterRecipes.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageIngots.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.tabPageTools.SuspendLayout();
            this.tabPageWeapons.SuspendLayout();
            this.tabPageArmour.SuspendLayout();
            this.tabPageBlocks.SuspendLayout();
            this.panelRecipes.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(141, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator6,
            this.helpToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "&New";
            this.newToolStripButton.Click += new System.EventHandler(this.newToolStripButton_Click);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Enabled = false;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "He&lp";
            // 
            // toolStripStatusLabelWorldName
            // 
            this.toolStripStatusLabelWorldName.Name = "toolStripStatusLabelWorldName";
            this.toolStripStatusLabelWorldName.Size = new System.Drawing.Size(122, 17);
            this.toolStripStatusLabelWorldName.Text = "No TFC world loaded.";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelWorldName,
            this.toolStripStatusLabelSpacer,
            this.toolStripStatusLabelSaveState});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelSpacer
            // 
            this.toolStripStatusLabelSpacer.Name = "toolStripStatusLabelSpacer";
            this.toolStripStatusLabelSpacer.Size = new System.Drawing.Size(647, 17);
            this.toolStripStatusLabelSpacer.Spring = true;
            // 
            // toolStripStatusLabelSaveState
            // 
            this.toolStripStatusLabelSaveState.Name = "toolStripStatusLabelSaveState";
            this.toolStripStatusLabelSaveState.Size = new System.Drawing.Size(0, 17);
            // 
            // panelWelcome
            // 
            this.panelWelcome.Controls.Add(this.labelRecent5);
            this.panelWelcome.Controls.Add(this.labelRecent4);
            this.panelWelcome.Controls.Add(this.labelRecent3);
            this.panelWelcome.Controls.Add(this.labelRecent2);
            this.panelWelcome.Controls.Add(this.labelRecent1);
            this.panelWelcome.Controls.Add(this.linkLabelRecent5);
            this.panelWelcome.Controls.Add(this.linkLabelRecent4);
            this.panelWelcome.Controls.Add(this.linkLabelRecent3);
            this.panelWelcome.Controls.Add(this.linkLabelRecent2);
            this.panelWelcome.Controls.Add(this.linkLabelRecent1);
            this.panelWelcome.Controls.Add(this.labelRecent);
            this.panelWelcome.Controls.Add(this.linkLabelOpenWorld);
            this.panelWelcome.Controls.Add(this.linkLabelNewWorld);
            this.panelWelcome.Controls.Add(this.labelStart);
            this.panelWelcome.Controls.Add(this.labelDescription);
            this.panelWelcome.Controls.Add(this.labelTitle);
            this.panelWelcome.Location = new System.Drawing.Point(0, 49);
            this.panelWelcome.Name = "panelWelcome";
            this.panelWelcome.Size = new System.Drawing.Size(784, 390);
            this.panelWelcome.TabIndex = 11;
            // 
            // labelRecent5
            // 
            this.labelRecent5.AutoSize = true;
            this.labelRecent5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelRecent5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.labelRecent5.Location = new System.Drawing.Point(106, 269);
            this.labelRecent5.Name = "labelRecent5";
            this.labelRecent5.Size = new System.Drawing.Size(115, 17);
            this.labelRecent5.TabIndex = 15;
            this.labelRecent5.Text = "Recent filepath 1";
            // 
            // labelRecent4
            // 
            this.labelRecent4.AutoSize = true;
            this.labelRecent4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelRecent4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.labelRecent4.Location = new System.Drawing.Point(106, 252);
            this.labelRecent4.Name = "labelRecent4";
            this.labelRecent4.Size = new System.Drawing.Size(115, 17);
            this.labelRecent4.TabIndex = 14;
            this.labelRecent4.Text = "Recent filepath 1";
            // 
            // labelRecent3
            // 
            this.labelRecent3.AutoSize = true;
            this.labelRecent3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelRecent3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.labelRecent3.Location = new System.Drawing.Point(106, 235);
            this.labelRecent3.Name = "labelRecent3";
            this.labelRecent3.Size = new System.Drawing.Size(115, 17);
            this.labelRecent3.TabIndex = 13;
            this.labelRecent3.Text = "Recent filepath 1";
            // 
            // labelRecent2
            // 
            this.labelRecent2.AutoSize = true;
            this.labelRecent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelRecent2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.labelRecent2.Location = new System.Drawing.Point(106, 218);
            this.labelRecent2.Name = "labelRecent2";
            this.labelRecent2.Size = new System.Drawing.Size(115, 17);
            this.labelRecent2.TabIndex = 12;
            this.labelRecent2.Text = "Recent filepath 1";
            // 
            // labelRecent1
            // 
            this.labelRecent1.AutoSize = true;
            this.labelRecent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelRecent1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.labelRecent1.Location = new System.Drawing.Point(106, 201);
            this.labelRecent1.Name = "labelRecent1";
            this.labelRecent1.Size = new System.Drawing.Size(115, 17);
            this.labelRecent1.TabIndex = 11;
            this.labelRecent1.Text = "Recent filepath 1";
            // 
            // linkLabelRecent5
            // 
            this.linkLabelRecent5.AutoSize = true;
            this.linkLabelRecent5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelRecent5.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelRecent5.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelRecent5.Location = new System.Drawing.Point(13, 269);
            this.linkLabelRecent5.Name = "linkLabelRecent5";
            this.linkLabelRecent5.Size = new System.Drawing.Size(87, 17);
            this.linkLabelRecent5.TabIndex = 10;
            this.linkLabelRecent5.TabStop = true;
            this.linkLabelRecent5.Text = "Recent file 5";
            this.linkLabelRecent5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecent_LinkClicked);
            // 
            // linkLabelRecent4
            // 
            this.linkLabelRecent4.AutoSize = true;
            this.linkLabelRecent4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelRecent4.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelRecent4.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelRecent4.Location = new System.Drawing.Point(13, 252);
            this.linkLabelRecent4.Name = "linkLabelRecent4";
            this.linkLabelRecent4.Size = new System.Drawing.Size(87, 17);
            this.linkLabelRecent4.TabIndex = 9;
            this.linkLabelRecent4.TabStop = true;
            this.linkLabelRecent4.Text = "Recent file 4";
            this.linkLabelRecent4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecent_LinkClicked);
            // 
            // linkLabelRecent3
            // 
            this.linkLabelRecent3.AutoSize = true;
            this.linkLabelRecent3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelRecent3.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelRecent3.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelRecent3.Location = new System.Drawing.Point(13, 235);
            this.linkLabelRecent3.Name = "linkLabelRecent3";
            this.linkLabelRecent3.Size = new System.Drawing.Size(87, 17);
            this.linkLabelRecent3.TabIndex = 8;
            this.linkLabelRecent3.TabStop = true;
            this.linkLabelRecent3.Text = "Recent file 3";
            this.linkLabelRecent3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecent_LinkClicked);
            // 
            // linkLabelRecent2
            // 
            this.linkLabelRecent2.AutoSize = true;
            this.linkLabelRecent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelRecent2.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelRecent2.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelRecent2.Location = new System.Drawing.Point(13, 218);
            this.linkLabelRecent2.Name = "linkLabelRecent2";
            this.linkLabelRecent2.Size = new System.Drawing.Size(87, 17);
            this.linkLabelRecent2.TabIndex = 7;
            this.linkLabelRecent2.TabStop = true;
            this.linkLabelRecent2.Text = "Recent file 2";
            this.linkLabelRecent2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecent_LinkClicked);
            // 
            // linkLabelRecent1
            // 
            this.linkLabelRecent1.AutoSize = true;
            this.linkLabelRecent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelRecent1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelRecent1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelRecent1.Location = new System.Drawing.Point(13, 201);
            this.linkLabelRecent1.Name = "linkLabelRecent1";
            this.linkLabelRecent1.Size = new System.Drawing.Size(87, 17);
            this.linkLabelRecent1.TabIndex = 6;
            this.linkLabelRecent1.TabStop = true;
            this.linkLabelRecent1.Text = "Recent file 1";
            this.linkLabelRecent1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecent_LinkClicked);
            // 
            // labelRecent
            // 
            this.labelRecent.AutoSize = true;
            this.labelRecent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelRecent.Location = new System.Drawing.Point(12, 181);
            this.labelRecent.Name = "labelRecent";
            this.labelRecent.Size = new System.Drawing.Size(61, 20);
            this.labelRecent.TabIndex = 5;
            this.labelRecent.Text = "Recent";
            // 
            // linkLabelOpenWorld
            // 
            this.linkLabelOpenWorld.AutoSize = true;
            this.linkLabelOpenWorld.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelOpenWorld.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelOpenWorld.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelOpenWorld.Location = new System.Drawing.Point(11, 140);
            this.linkLabelOpenWorld.Name = "linkLabelOpenWorld";
            this.linkLabelOpenWorld.Size = new System.Drawing.Size(80, 17);
            this.linkLabelOpenWorld.TabIndex = 4;
            this.linkLabelOpenWorld.TabStop = true;
            this.linkLabelOpenWorld.Text = "Open world";
            this.linkLabelOpenWorld.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOpenWorld_LinkClicked);
            // 
            // linkLabelNewWorld
            // 
            this.linkLabelNewWorld.AutoSize = true;
            this.linkLabelNewWorld.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.linkLabelNewWorld.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabelNewWorld.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(255)))));
            this.linkLabelNewWorld.Location = new System.Drawing.Point(11, 123);
            this.linkLabelNewWorld.Name = "linkLabelNewWorld";
            this.linkLabelNewWorld.Size = new System.Drawing.Size(72, 17);
            this.linkLabelNewWorld.TabIndex = 3;
            this.linkLabelNewWorld.TabStop = true;
            this.linkLabelNewWorld.Text = "New world";
            this.linkLabelNewWorld.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelNewWorld_LinkClicked);
            // 
            // labelStart
            // 
            this.labelStart.AutoSize = true;
            this.labelStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelStart.Location = new System.Drawing.Point(10, 103);
            this.labelStart.Name = "labelStart";
            this.labelStart.Size = new System.Drawing.Size(44, 20);
            this.labelStart.TabIndex = 2;
            this.labelStart.Text = "Start";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelDescription.Location = new System.Drawing.Point(8, 52);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(143, 24);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "A TFC Assistant";
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.labelTitle.Location = new System.Drawing.Point(6, 16);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(253, 36);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "AssistantSmithers";
            // 
            // contextMenuStripRecipePanel
            // 
            this.contextMenuStripRecipePanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemViewRecipeNotes,
            this.toolStripMenuItemEditRecipe,
            this.toolStripMenuItemDeleteRecipe});
            this.contextMenuStripRecipePanel.Name = "contextMenuStripRecipePanel";
            this.contextMenuStripRecipePanel.Size = new System.Drawing.Size(170, 70);
            this.contextMenuStripRecipePanel.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripRecipePanel_Opening);
            // 
            // toolStripMenuItemViewRecipeNotes
            // 
            this.toolStripMenuItemViewRecipeNotes.Name = "toolStripMenuItemViewRecipeNotes";
            this.toolStripMenuItemViewRecipeNotes.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemViewRecipeNotes.Text = "View Notes";
            this.toolStripMenuItemViewRecipeNotes.Click += new System.EventHandler(this.toolStripMenuItemViewRecipeNotes_Click);
            // 
            // toolStripMenuItemEditRecipe
            // 
            this.toolStripMenuItemEditRecipe.Name = "toolStripMenuItemEditRecipe";
            this.toolStripMenuItemEditRecipe.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemEditRecipe.Text = "Edit Recipe";
            this.toolStripMenuItemEditRecipe.Click += new System.EventHandler(this.toolStripMenuItemEditRecipe_Click);
            // 
            // toolStripMenuItemDeleteRecipe
            // 
            this.toolStripMenuItemDeleteRecipe.Name = "toolStripMenuItemDeleteRecipe";
            this.toolStripMenuItemDeleteRecipe.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.toolStripMenuItemDeleteRecipe.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemDeleteRecipe.Text = "Delete Recipe";
            this.toolStripMenuItemDeleteRecipe.Click += new System.EventHandler(this.toolStripMenuItemDeleteRecipe_Click);
            // 
            // groupBoxFilterRecipes
            // 
            this.groupBoxFilterRecipes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFilterRecipes.Controls.Add(this.labelFilterMaterial);
            this.groupBoxFilterRecipes.Controls.Add(this.labelSearch);
            this.groupBoxFilterRecipes.Controls.Add(this.comboBoxFilterMaterial);
            this.groupBoxFilterRecipes.Controls.Add(this.textBoxSearch);
            this.groupBoxFilterRecipes.Location = new System.Drawing.Point(12, 3);
            this.groupBoxFilterRecipes.Name = "groupBoxFilterRecipes";
            this.groupBoxFilterRecipes.Size = new System.Drawing.Size(760, 61);
            this.groupBoxFilterRecipes.TabIndex = 6;
            this.groupBoxFilterRecipes.TabStop = false;
            this.groupBoxFilterRecipes.Text = "Filter Recipes";
            // 
            // labelFilterMaterial
            // 
            this.labelFilterMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFilterMaterial.AutoSize = true;
            this.labelFilterMaterial.Location = new System.Drawing.Point(564, 24);
            this.labelFilterMaterial.Name = "labelFilterMaterial";
            this.labelFilterMaterial.Size = new System.Drawing.Size(63, 13);
            this.labelFilterMaterial.TabIndex = 8;
            this.labelFilterMaterial.Text = "Metal Type:";
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Location = new System.Drawing.Point(6, 24);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(44, 13);
            this.labelSearch.TabIndex = 7;
            this.labelSearch.Text = "Search:";
            // 
            // comboBoxFilterMaterial
            // 
            this.comboBoxFilterMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxFilterMaterial.FormattingEnabled = true;
            this.comboBoxFilterMaterial.Location = new System.Drawing.Point(633, 21);
            this.comboBoxFilterMaterial.Name = "comboBoxFilterMaterial";
            this.comboBoxFilterMaterial.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilterMaterial.TabIndex = 6;
            this.comboBoxFilterMaterial.SelectedIndexChanged += new System.EventHandler(this.comboBoxFilterMaterial_SelectedIndexChanged);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(56, 21);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(100, 20);
            this.textBoxSearch.TabIndex = 3;
            this.textBoxSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxSearch_KeyUp);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageIngots);
            this.tabControl1.Controls.Add(this.tabPageGeneral);
            this.tabControl1.Controls.Add(this.tabPageTools);
            this.tabControl1.Controls.Add(this.tabPageWeapons);
            this.tabControl1.Controls.Add(this.tabPageArmour);
            this.tabControl1.Controls.Add(this.tabPageBlocks);
            this.tabControl1.Location = new System.Drawing.Point(12, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(760, 287);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPageIngots
            // 
            this.tabPageIngots.Controls.Add(this.recipeIngotsContainer);
            this.tabPageIngots.Location = new System.Drawing.Point(4, 22);
            this.tabPageIngots.Name = "tabPageIngots";
            this.tabPageIngots.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIngots.Size = new System.Drawing.Size(752, 261);
            this.tabPageIngots.TabIndex = 0;
            this.tabPageIngots.Text = "Ingots";
            this.tabPageIngots.UseVisualStyleBackColor = true;
            // 
            // recipeIngotsContainer
            // 
            this.recipeIngotsContainer.AutoScroll = true;
            this.recipeIngotsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeIngotsContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeIngotsContainer.Name = "recipeIngotsContainer";
            this.recipeIngotsContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeIngotsContainer.TabIndex = 0;
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Controls.Add(this.recipeGeneralContainer);
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneral.Size = new System.Drawing.Size(752, 261);
            this.tabPageGeneral.TabIndex = 1;
            this.tabPageGeneral.Text = "General";
            this.tabPageGeneral.UseVisualStyleBackColor = true;
            // 
            // recipeGeneralContainer
            // 
            this.recipeGeneralContainer.AutoScroll = true;
            this.recipeGeneralContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeGeneralContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeGeneralContainer.Name = "recipeGeneralContainer";
            this.recipeGeneralContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeGeneralContainer.TabIndex = 0;
            // 
            // tabPageTools
            // 
            this.tabPageTools.Controls.Add(this.recipeToolsContainer);
            this.tabPageTools.Location = new System.Drawing.Point(4, 22);
            this.tabPageTools.Name = "tabPageTools";
            this.tabPageTools.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTools.Size = new System.Drawing.Size(752, 261);
            this.tabPageTools.TabIndex = 2;
            this.tabPageTools.Text = "Tools";
            this.tabPageTools.UseVisualStyleBackColor = true;
            // 
            // recipeToolsContainer
            // 
            this.recipeToolsContainer.AutoScroll = true;
            this.recipeToolsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeToolsContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeToolsContainer.Name = "recipeToolsContainer";
            this.recipeToolsContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeToolsContainer.TabIndex = 0;
            // 
            // tabPageWeapons
            // 
            this.tabPageWeapons.Controls.Add(this.recipeWeaponsContainer);
            this.tabPageWeapons.Location = new System.Drawing.Point(4, 22);
            this.tabPageWeapons.Name = "tabPageWeapons";
            this.tabPageWeapons.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWeapons.Size = new System.Drawing.Size(752, 261);
            this.tabPageWeapons.TabIndex = 3;
            this.tabPageWeapons.Text = "Weapons";
            this.tabPageWeapons.UseVisualStyleBackColor = true;
            // 
            // recipeWeaponsContainer
            // 
            this.recipeWeaponsContainer.AutoScroll = true;
            this.recipeWeaponsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeWeaponsContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeWeaponsContainer.Name = "recipeWeaponsContainer";
            this.recipeWeaponsContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeWeaponsContainer.TabIndex = 0;
            // 
            // tabPageArmour
            // 
            this.tabPageArmour.Controls.Add(this.recipeArmourContainer);
            this.tabPageArmour.Location = new System.Drawing.Point(4, 22);
            this.tabPageArmour.Name = "tabPageArmour";
            this.tabPageArmour.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageArmour.Size = new System.Drawing.Size(752, 261);
            this.tabPageArmour.TabIndex = 4;
            this.tabPageArmour.Text = "Armour";
            this.tabPageArmour.UseVisualStyleBackColor = true;
            // 
            // recipeArmourContainer
            // 
            this.recipeArmourContainer.AutoScroll = true;
            this.recipeArmourContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeArmourContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeArmourContainer.Name = "recipeArmourContainer";
            this.recipeArmourContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeArmourContainer.TabIndex = 0;
            // 
            // tabPageBlocks
            // 
            this.tabPageBlocks.Controls.Add(this.recipeBlocksContainer);
            this.tabPageBlocks.Location = new System.Drawing.Point(4, 22);
            this.tabPageBlocks.Name = "tabPageBlocks";
            this.tabPageBlocks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBlocks.Size = new System.Drawing.Size(752, 261);
            this.tabPageBlocks.TabIndex = 5;
            this.tabPageBlocks.Text = "Blocks";
            this.tabPageBlocks.UseVisualStyleBackColor = true;
            // 
            // recipeBlocksContainer
            // 
            this.recipeBlocksContainer.AutoScroll = true;
            this.recipeBlocksContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeBlocksContainer.Location = new System.Drawing.Point(3, 3);
            this.recipeBlocksContainer.Name = "recipeBlocksContainer";
            this.recipeBlocksContainer.Size = new System.Drawing.Size(746, 255);
            this.recipeBlocksContainer.TabIndex = 0;
            // 
            // buttonDeleteRecipe
            // 
            this.buttonDeleteRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteRecipe.Enabled = false;
            this.buttonDeleteRecipe.Location = new System.Drawing.Point(542, 363);
            this.buttonDeleteRecipe.Name = "buttonDeleteRecipe";
            this.buttonDeleteRecipe.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteRecipe.TabIndex = 10;
            this.buttonDeleteRecipe.Text = "Delete";
            this.buttonDeleteRecipe.UseVisualStyleBackColor = true;
            this.buttonDeleteRecipe.Click += new System.EventHandler(this.buttonDeleteRecipe_Click);
            // 
            // buttonEditRecipe
            // 
            this.buttonEditRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditRecipe.Enabled = false;
            this.buttonEditRecipe.Location = new System.Drawing.Point(623, 363);
            this.buttonEditRecipe.Name = "buttonEditRecipe";
            this.buttonEditRecipe.Size = new System.Drawing.Size(75, 23);
            this.buttonEditRecipe.TabIndex = 9;
            this.buttonEditRecipe.Text = "Edit";
            this.buttonEditRecipe.UseVisualStyleBackColor = true;
            this.buttonEditRecipe.Click += new System.EventHandler(this.buttonEditRecipe_Click);
            // 
            // buttonAddRecipe
            // 
            this.buttonAddRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddRecipe.Location = new System.Drawing.Point(704, 363);
            this.buttonAddRecipe.Name = "buttonAddRecipe";
            this.buttonAddRecipe.Size = new System.Drawing.Size(75, 23);
            this.buttonAddRecipe.TabIndex = 8;
            this.buttonAddRecipe.Text = "Add";
            this.buttonAddRecipe.UseVisualStyleBackColor = true;
            this.buttonAddRecipe.Click += new System.EventHandler(this.buttonAddRecipe_Click);
            // 
            // panelRecipes
            // 
            this.panelRecipes.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelRecipes.Controls.Add(this.buttonAddRecipe);
            this.panelRecipes.Controls.Add(this.buttonEditRecipe);
            this.panelRecipes.Controls.Add(this.buttonDeleteRecipe);
            this.panelRecipes.Controls.Add(this.tabControl1);
            this.panelRecipes.Controls.Add(this.groupBoxFilterRecipes);
            this.panelRecipes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRecipes.Enabled = false;
            this.panelRecipes.Location = new System.Drawing.Point(0, 49);
            this.panelRecipes.Name = "panelRecipes";
            this.panelRecipes.Size = new System.Drawing.Size(784, 390);
            this.panelRecipes.TabIndex = 12;
            this.panelRecipes.Visible = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelWelcome);
            this.Controls.Add(this.panelRecipes);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "MainWindow";
            this.Text = "AssistantSmithers: A TFC Assistant";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panelWelcome.ResumeLayout(false);
            this.panelWelcome.PerformLayout();
            this.contextMenuStripRecipePanel.ResumeLayout(false);
            this.groupBoxFilterRecipes.ResumeLayout(false);
            this.groupBoxFilterRecipes.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageIngots.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.tabPageTools.ResumeLayout(false);
            this.tabPageWeapons.ResumeLayout(false);
            this.tabPageArmour.ResumeLayout(false);
            this.tabPageBlocks.ResumeLayout(false);
            this.panelRecipes.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelWorldName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripRecipePanel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemViewRecipeNotes;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEditRecipe;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDeleteRecipe;
        private System.Windows.Forms.Panel panelWelcome;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelRecent;
        private System.Windows.Forms.LinkLabel linkLabelOpenWorld;
        private System.Windows.Forms.LinkLabel linkLabelNewWorld;
        private System.Windows.Forms.Label labelStart;
        private System.Windows.Forms.GroupBox groupBoxFilterRecipes;
        private System.Windows.Forms.Label labelFilterMaterial;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.ComboBox comboBoxFilterMaterial;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageIngots;
        private System.Windows.Forms.FlowLayoutPanel recipeIngotsContainer;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.FlowLayoutPanel recipeGeneralContainer;
        private System.Windows.Forms.TabPage tabPageTools;
        private System.Windows.Forms.FlowLayoutPanel recipeToolsContainer;
        private System.Windows.Forms.TabPage tabPageWeapons;
        private System.Windows.Forms.FlowLayoutPanel recipeWeaponsContainer;
        private System.Windows.Forms.TabPage tabPageArmour;
        private System.Windows.Forms.FlowLayoutPanel recipeArmourContainer;
        private System.Windows.Forms.TabPage tabPageBlocks;
        private System.Windows.Forms.FlowLayoutPanel recipeBlocksContainer;
        private System.Windows.Forms.Button buttonDeleteRecipe;
        private System.Windows.Forms.Button buttonEditRecipe;
        private System.Windows.Forms.Button buttonAddRecipe;
        private System.Windows.Forms.Panel panelRecipes;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSpacer;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSaveState;
        private System.Windows.Forms.LinkLabel linkLabelRecent5;
        private System.Windows.Forms.LinkLabel linkLabelRecent4;
        private System.Windows.Forms.LinkLabel linkLabelRecent3;
        private System.Windows.Forms.LinkLabel linkLabelRecent2;
        private System.Windows.Forms.LinkLabel linkLabelRecent1;
        private System.Windows.Forms.Label labelRecent5;
        private System.Windows.Forms.Label labelRecent4;
        private System.Windows.Forms.Label labelRecent3;
        private System.Windows.Forms.Label labelRecent2;
        private System.Windows.Forms.Label labelRecent1;
    }
}

