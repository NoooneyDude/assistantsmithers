﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistantSmithers
{
    public partial class NewWorldDialog : Form
    {
        public NewWorldDialog()
        {
            InitializeComponent();
        }

        public string getWorldName()
        {
            return textBoxWorldName.Text;
        }
    }
}
