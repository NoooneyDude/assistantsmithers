﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistantSmithers
{
    public partial class MainWindow : Form
    {
        Size recipePanelSize = new Size(160, 160);
        Panel lastSelectedRecipePanel;
        Panel selectedRecipePanel;
        Recipe selectedRecipe;

        public MainWindow()
        {
            InitializeComponent();

            LoadSettings();

            Metal pseudoMetal = new Metal();
            pseudoMetal.Name = "All metals";
            List<Metal> metals = Metal.GetMetals();
            metals.Insert(0, pseudoMetal);

            comboBoxFilterMaterial.DataSource = metals;
            comboBoxFilterMaterial.DisplayMember = "Name";
            comboBoxFilterMaterial.SelectedIndex = 0;

            tabPageIngots.Tag = ItemType.Ingot;
            tabPageGeneral.Tag = ItemType.General;
            tabPageTools.Tag = ItemType.Tool;
            tabPageWeapons.Tag = ItemType.Weapon;
            tabPageArmour.Tag = ItemType.Armour;
            tabPageBlocks.Tag = ItemType.Block;
        }

        private void LoadSettings()
        {
            if (Settings.GetDarkMode())
            {
                SetAppearanceToDarkMode();
            }
            else
            {
                SetAppearanceToDefault();
            }

            LoadRecentFileList();
        }

        private void LoadRecentFileList()
        {
            for (int i = 0; i < 5; i++)
            {
                string filePath = Settings.GetRecentFile(i);

                if (filePath == "<blank>") filePath = "";

                LinkLabel linkLabelRecent = (LinkLabel) panelWelcome.Controls["linkLabelRecent" + (i + 1)];
                Label labelRecent = (Label) panelWelcome.Controls["labelRecent" + (i + 1)];
                
                linkLabelRecent.Text = System.IO.Path.GetFileNameWithoutExtension(filePath);
                linkLabelRecent.Tag = filePath;
                labelRecent.Text = filePath;
                labelRecent.Left = linkLabelRecent.Width + 15;
            }
        }

        private void SetAppearanceToDarkMode()
        {
            if (!Settings.GetDarkMode()) return;

            // Common
            BackColor = Color.FromArgb(30, 30, 30);
            ToolStripManager.Renderer = new ToolStripDarkRenderer();

            menuStrip1.BackColor = Color.FromArgb(60, 60, 60);
            menuStrip1.ForeColor = Color.FromArgb(200, 200, 200);
            toolStrip1.BackColor = Color.FromArgb(50, 50, 50);
            // statusStrip1.BackColor = Color.FromArgb(37, 37, 37); // Dull gray
            statusStrip1.BackColor = Color.FromArgb(104, 33, 122); // EPIC PURPLE!
            toolStripStatusLabelWorldName.ForeColor = Color.FromArgb(204, 204, 204);
            toolStripStatusLabelSaveState.ForeColor = Color.FromArgb(204, 204, 204);

            // Welcome
            labelTitle.ForeColor = Color.FromArgb(200, 200, 200);
            labelDescription.ForeColor = Color.FromArgb(128, 128, 128);
            labelStart.ForeColor = Color.FromArgb(150, 150, 150);
            labelRecent.ForeColor = Color.FromArgb(150, 150, 150);

            linkLabelNewWorld.LinkColor = Color.FromArgb(55, 148, 255);
            linkLabelOpenWorld.LinkColor = Color.FromArgb(55, 148, 255);

            linkLabelRecent1.LinkColor = Color.FromArgb(55, 148, 255);
            linkLabelRecent2.LinkColor = Color.FromArgb(55, 148, 255);
            linkLabelRecent3.LinkColor = Color.FromArgb(55, 148, 255);
            linkLabelRecent4.LinkColor = Color.FromArgb(55, 148, 255);
            linkLabelRecent5.LinkColor = Color.FromArgb(55, 148, 255);

            // Recipes
            groupBoxFilterRecipes.Paint += groupBoxFilterRecipes_Paint;
            groupBoxFilterRecipes.ForeColor = Color.FromArgb(60, 60, 60); // groupBox border colour (custom paint)
            groupBoxFilterRecipes.BackColor = Color.FromArgb(30, 30, 30);

            labelSearch.ForeColor = Color.FromArgb(187, 187, 187);
            labelFilterMaterial.ForeColor = Color.FromArgb(187, 187, 187);
            textBoxSearch.BackColor = Color.FromArgb(60, 60, 60);
            textBoxSearch.ForeColor = Color.FromArgb(204, 204, 204);
            textBoxSearch.BorderStyle = BorderStyle.FixedSingle;
            comboBoxFilterMaterial.BackColor = Color.FromArgb(60, 60, 60);
            comboBoxFilterMaterial.ForeColor = Color.FromArgb(204, 204, 204);
            comboBoxFilterMaterial.FlatStyle = FlatStyle.Popup;

            //tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;
            //tabPageIngots.BackColor = Color.FromArgb(30, 30, 30);
            recipeIngotsContainer.BackColor = Color.FromArgb(30, 30, 30);
            recipeGeneralContainer.BackColor = Color.FromArgb(30, 30, 30);
            recipeToolsContainer.BackColor = Color.FromArgb(30, 30, 30);
            recipeWeaponsContainer.BackColor = Color.FromArgb(30, 30, 30);
            recipeArmourContainer.BackColor = Color.FromArgb(30, 30, 30);
            recipeBlocksContainer.BackColor = Color.FromArgb(30, 30, 30);

            buttonAddRecipe.BackColor = Color.FromArgb(60, 60, 60);
            buttonEditRecipe.BackColor = Color.FromArgb(60, 60, 60);
            buttonDeleteRecipe.BackColor = Color.FromArgb(60, 60, 60);
            buttonAddRecipe.ForeColor = Color.FromArgb(200, 200, 200);
            buttonEditRecipe.ForeColor = Color.FromArgb(200, 200, 200);
            buttonDeleteRecipe.ForeColor = Color.FromArgb(200, 200, 200);
            buttonAddRecipe.FlatStyle = FlatStyle.Flat;
            buttonEditRecipe.FlatStyle = FlatStyle.Flat;
            buttonDeleteRecipe.FlatStyle = FlatStyle.Flat;

            ResetRecipeContainers(); // Easier to redraw the recipe panels with the new color
            AddRecipePanels();
        }

        private void SetAppearanceToDefault()
        {
            // Common
            BackColor = DefaultBackColor;
            ToolStripManager.Renderer = new ToolStripProfessionalRenderer();

            menuStrip1.BackColor = DefaultBackColor;
            menuStrip1.ForeColor = DefaultForeColor;
            toolStrip1.BackColor = DefaultBackColor;
            statusStrip1.BackColor = DefaultBackColor;
            toolStripStatusLabelWorldName.ForeColor = DefaultForeColor;
            toolStripStatusLabelSaveState.ForeColor = DefaultForeColor;

            // Welcome
            labelTitle.ForeColor = DefaultForeColor;
            labelDescription.ForeColor = DefaultForeColor;
            labelStart.ForeColor = DefaultForeColor;
            labelRecent.ForeColor = DefaultForeColor;

            linkLabelNewWorld.LinkColor = Color.FromArgb(50, 50, 255);
            linkLabelOpenWorld.LinkColor = Color.FromArgb(50, 50, 255);

            linkLabelRecent1.LinkColor = Color.FromArgb(50, 50, 255);
            linkLabelRecent2.LinkColor = Color.FromArgb(50, 50, 255);
            linkLabelRecent3.LinkColor = Color.FromArgb(50, 50, 255);
            linkLabelRecent4.LinkColor = Color.FromArgb(50, 50, 255);
            linkLabelRecent5.LinkColor = Color.FromArgb(50, 50, 255);

            // Recipes
            groupBoxFilterRecipes.Paint -= groupBoxFilterRecipes_Paint;
            groupBoxFilterRecipes.ForeColor = DefaultForeColor;
            groupBoxFilterRecipes.BackColor = DefaultBackColor;

            labelSearch.ForeColor = DefaultForeColor;
            labelFilterMaterial.ForeColor = DefaultForeColor;
            textBoxSearch.BackColor = DefaultBackColor;
            textBoxSearch.ForeColor = DefaultForeColor;
            textBoxSearch.BorderStyle = BorderStyle.Fixed3D;
            comboBoxFilterMaterial.BackColor = DefaultBackColor;
            comboBoxFilterMaterial.ForeColor = DefaultForeColor;
            comboBoxFilterMaterial.FlatStyle = FlatStyle.Standard;
            tabPageIngots.BackColor = DefaultBackColor;

            recipeIngotsContainer.BackColor = DefaultBackColor;
            recipeGeneralContainer.BackColor = DefaultBackColor;
            recipeToolsContainer.BackColor = DefaultBackColor;
            recipeWeaponsContainer.BackColor = DefaultBackColor;
            recipeArmourContainer.BackColor = DefaultBackColor;
            recipeBlocksContainer.BackColor = DefaultBackColor;

            buttonAddRecipe.BackColor = DefaultBackColor;
            buttonEditRecipe.BackColor = DefaultBackColor;
            buttonDeleteRecipe.BackColor = DefaultBackColor;
            buttonAddRecipe.ForeColor = DefaultForeColor;
            buttonEditRecipe.ForeColor = DefaultForeColor;
            buttonDeleteRecipe.ForeColor = DefaultForeColor;
            buttonAddRecipe.FlatStyle = FlatStyle.Standard;
            buttonEditRecipe.FlatStyle = FlatStyle.Standard;
            buttonDeleteRecipe.FlatStyle = FlatStyle.Standard;

            ResetRecipeContainers(); // Easier to redraw the recipe panels with the new color
            AddRecipePanels();
        }

        private void PromptForSave()
        {
            // Shouldn't really be using an output for determining current state...
            if (toolStripStatusLabelSaveState.Text == "Unsaved")
            {
                DialogResult confirmResult = MessageBox.Show("Do you want to save the recipes for the current world?",
                    "Confirm New World",
                    MessageBoxButtons.YesNo);

                if (confirmResult == DialogResult.Yes) SaveRecipeFile();
            }
        }

        private void ResetRecipeContainers()
        {
            recipeIngotsContainer.Controls.Clear();
            recipeGeneralContainer.Controls.Clear();
            recipeToolsContainer.Controls.Clear();
            recipeWeaponsContainer.Controls.Clear();
            recipeArmourContainer.Controls.Clear();
            recipeBlocksContainer.Controls.Clear();
        }

        private void ShowRecipePanel()
        {
            panelWelcome.Visible = false;
            panelWelcome.Enabled = false;
            panelWelcome.SendToBack();

            panelRecipes.Visible = true;
            panelRecipes.Enabled = true;

            saveToolStripMenuItem.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            saveToolStripButton.Enabled = true;
        }

        private void NewRecipeFile()
        {
            PromptForSave();

            NewWorldDialog newWorldWindow = new NewWorldDialog();
            DialogResult dr = newWorldWindow.ShowDialog(this);

            if (dr == DialogResult.OK)
            {
                World.Reset();
                ResetRecipeContainers();

                if (string.IsNullOrWhiteSpace(newWorldWindow.getWorldName())) World.Name = "<new world>";
                else World.Name = newWorldWindow.getWorldName();

                toolStripStatusLabelWorldName.Text = "Selected world: " + World.Name;
                Text = "AssistantSmithers: " + World.Name;
                toolStripStatusLabelSaveState.Text = "Unsaved";

                ShowRecipePanel();
            }
        }

        private void OpenLoadFileWindow()
        {
            PromptForSave();

            using (OpenFileDialog openFileDialogRecipeFile = new OpenFileDialog())
            {
                openFileDialogRecipeFile.Filter = "AssistantSmithers files (*.ass)|*.ass|XML files (*.xml) | *.xml|All files (*.*)|*.*";
                openFileDialogRecipeFile.FilterIndex = 2;

                if (openFileDialogRecipeFile.ShowDialog() == DialogResult.OK)
                {
                    World.Reset();
                    World.File = openFileDialogRecipeFile.FileName;
                    OpenRecipeFile();
                }
            }
        }

        private void SaveNewRecipeFile()
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                //saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft";
                saveFileDialog.FileName = World.Name + "_AnvilRecipes.xml";
                saveFileDialog.Filter = "AssistantSmithers file (*.ass)|*.ass|XML file (*.xml) | *.xml";
                saveFileDialog.FilterIndex = 2;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    World.File = saveFileDialog.FileName;
                    SaveRecipeFile();
                }
            }
        }

        private void SaveRecipeFile()
        {
            if (string.IsNullOrWhiteSpace(World.File))
            {
                SaveNewRecipeFile();
            }
            else
            {
                XMLOperations.GenerateXmlDocument();
                XMLOperations.SaveAllRecipes();
                XMLOperations.SaveXMLFile();

                toolStripStatusLabelSaveState.Text = "Saved";
                Settings.UpdateTopRecentFile(World.File);
            }
        }

        private void OpenOptionsWindow()
        {
            OptionsWindow optionsWindow = new OptionsWindow();
            optionsWindow.ShowDialog();
            LoadSettings();
        }

        private void OpenRecipeFile()
        {
            ResetRecipeContainers();
            Settings.UpdateTopRecentFile(World.File);
            string LoadXMLFileAttemptMessage = XMLOperations.LoadXMLFile();


            if (LoadXMLFileAttemptMessage == "File doesn't exist.")
            {
                MessageBox.Show("Failed to load file: it does not appear to exist.",
                    "Load File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Settings.RemoveRecentFile(World.File);
                LoadRecentFileList();
                return;
            }

            if (LoadXMLFileAttemptMessage == "File not valid XML.")
            {
                MessageBox.Show("Failed to load file: it does not appear to be valid XML.",
                    "Load File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (LoadXMLFileAttemptMessage == "File not valid recipe file.")
            {
                MessageBox.Show("Failed to load file: it does not appear to be a valid recipe file.",
                    "Load File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (LoadXMLFileAttemptMessage == "Failed to load file.")
            {
                MessageBox.Show("Failed to load file.",
                    "Load File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            foreach (Recipe recipe in World.Recipes) AddRecipePanel(recipe);

            // Not even gonna confirm if recipes were actually successfully loaded, eh?
            toolStripStatusLabelWorldName.Text = "Selected world: " + World.Name;
            Text = "AssistantSmithers: " + World.Name;
            toolStripStatusLabelSaveState.Text = "Saved";

            ShowRecipePanel();
        }

        private Panel CreateRecipePanel(Recipe recipe) // Create the panel of the recipe (won't display panel)
        {
            Panel recipePanel = new Panel
            {
                Name = "RecipePanel" + recipe.Material.Name.Trim() + recipe.Name.Trim(),
                Tag = recipe,
                ContextMenuStrip = contextMenuStripRecipePanel,
                BorderStyle = BorderStyle.FixedSingle,
                Padding = new Padding(0, 0, 0, 5),
            };

            Label recipeName = new Label
            {
                Name = "RecipeName",
                Location = new Point(5, 4),
                AutoSize = true,
                Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point),
                Text = recipe.Material.Name + " " + recipe.Name
            };
            // overwrite name for this special instance
            if (recipe.ItemType == ItemType.Ingot) recipeName.Text = recipe.Name + " " + recipe.Material.Name;
            recipePanel.Controls.Add(recipeName);

            Label recipeRules = new Label
            {
                Name = "RecipeRules",
                Location = new Point(5, 32),
                AutoSize = true,
                Text = "Rules: " + recipe.RuleOne.Name + ", " + recipe.RuleTwo.Name + ", " + recipe.RuleThree.Name
            };
            recipePanel.Controls.Add(recipeRules);
            recipePanel.GotFocus += new EventHandler(recipePanel_GotFocus);
            recipePanel.LostFocus += new EventHandler(recipePanel_LostFocus);
            recipePanel.Click += new EventHandler(recipePanel_Click);
            recipePanel.Paint += new PaintEventHandler(recipePanel_Paint);
            (recipePanel as Control).KeyUp += new KeyEventHandler(recipePanel_KeyUp);

            if (Settings.GetDarkMode())
            {
                recipePanel.BorderStyle = BorderStyle.None;
                recipePanel.BackColor = Color.FromArgb(24, 24, 24);
                recipeName.ForeColor = Color.FromArgb(204, 204, 204);
                recipeRules.ForeColor = Color.FromArgb(150, 150, 150);
            }

            for (int i = 0; i < recipe.Steps.Count; i++)
            {
                Label recipeStep = new Label();
                recipeStep.Name = "RecipeStep" + i;
                recipeStep.Location = new Point(5, 55 + (20 * i));
                recipeStep.AutoSize = true;
                recipeStep.Font = new Font("Microsoft Sans Serif", 10F);
                recipeStep.Text = recipe.Steps[i].Value + " " + recipe.Steps[i].Key.Name;
                recipePanel.Controls.Add(recipeStep);

                if (Settings.GetDarkMode()) recipeStep.ForeColor = Color.FromArgb(150, 150, 150);
            }

            return recipePanel;
        }

        private void AddRecipePanel(Recipe recipe) // Create a recipe panel, then add it to its relevant container.
        {
            Panel recipePanel = CreateRecipePanel(recipe);
            FlowLayoutPanel recipeContainer;

            if (recipe.ItemType == ItemType.Ingot) recipeContainer = recipeIngotsContainer;
            else if (recipe.ItemType == ItemType.Tool) recipeContainer = recipeToolsContainer;
            else if (recipe.ItemType == ItemType.Weapon) recipeContainer = recipeWeaponsContainer;
            else if (recipe.ItemType == ItemType.Armour) recipeContainer = recipeArmourContainer;
            else if (recipe.ItemType == ItemType.Block) recipeContainer = recipeBlocksContainer;
            else recipeContainer = recipeGeneralContainer; // default container

            recipeContainer.Controls.Add(recipePanel);

            recipePanel.AutoSize = true;
            bool resizeExisting = false;
            // If this new panel bigger than all the others, update all the others to be as big.
            if (recipePanel.Width > recipePanelSize.Width)
            {
                recipePanelSize = new Size(recipePanel.Width, recipePanelSize.Height);
                resizeExisting = true;
            }
            if (recipePanel.Height > recipePanelSize.Height)
            {
                recipePanelSize = new Size(recipePanelSize.Width, recipePanel.Height);
                resizeExisting = true;
            }

            recipePanel.AutoSize = false;
            if (resizeExisting) ResizeRecipePanels(recipeContainer.Controls); // bring the rest up to the new standard
            else recipePanel.Size = recipePanelSize; // bring it up to standard
        }

        private void FilterRecipes()
        {
            ResetRecipeContainers();
            Metal filterMaterial = (Metal) comboBoxFilterMaterial.SelectedItem;
            string[] searchTerms = textBoxSearch.Text.Trim().ToLower().Split();

            foreach (Recipe recipe in World.Recipes)
            {
                if (comboBoxFilterMaterial.SelectedIndex != 0 && recipe.Material != filterMaterial) continue;

                if (textBoxSearch.Text.Length == 0)
                {
                    AddRecipePanel(recipe);
                    continue;
                }

                string[] recipeNameTerms = recipe.Name.Trim().ToLower().Split();
                string[] recipeMaterialTerms = recipe.Material.Name.Trim().ToLower().Split();
                if (searchTerms.Any(recipeNameTerms.Contains) || searchTerms.Any(recipeMaterialTerms.Contains))
                {
                    AddRecipePanel(recipe);
                }
            }
        }

        private void AddRecipePanels()
        {
            FilterRecipes();
        }

        private void AddRecipe() // Open blank Recipe Window
        {
            RecipeDialog recipeWindow = new RecipeDialog();

            ComboBox recipeItemType = (ComboBox) recipeWindow.Controls["comboBoxRecipeCategory"];
            ComboBox recipeMaterial = (ComboBox) recipeWindow.Controls["comboBoxRecipeMaterial"];
            
            recipeItemType.SelectedItem = tabControl1.SelectedTab.Tag;

            if (comboBoxFilterMaterial.SelectedIndex != 0)
            {
                recipeMaterial.SelectedItem = comboBoxFilterMaterial.SelectedItem;
            } else
            {
                recipeMaterial.SelectedItem = World.GetCurrentAgeMetal();
            }

            DialogResult dr = recipeWindow.ShowDialog(this);
            if (dr == DialogResult.OK)
            {
                Recipe recipe = recipeWindow.getRecipe();
                World.Recipes.Add(recipe);
                recipeWindow.Close();
                AddRecipePanel(recipe);

                toolStripStatusLabelSaveState.Text = "Unsaved";
            }
        }

        private void EditRecipe() // Open filled Recipe Window
        {
            RecipeDialog recipeWindow = new RecipeDialog(selectedRecipe);
            DialogResult dr = recipeWindow.ShowDialog(this);
            if (dr == DialogResult.OK)
            {
                World.Recipes.Remove(selectedRecipe);
                lastSelectedRecipePanel.Dispose();

                Recipe recipe = recipeWindow.getRecipe();
                recipeWindow.Close();
                World.Recipes.Add(recipe);
                AddRecipePanel(recipe);

                toolStripStatusLabelSaveState.Text = "Unsaved";
            }
        }

        private void DeleteRecipe()
        {
            World.Recipes.Remove(selectedRecipe);
            lastSelectedRecipePanel.Dispose();
            toolStripStatusLabelSaveState.Text = "Unsaved";
        }

        private void ResizeRecipePanels(Control.ControlCollection controls)
        {
            for (int i = 0; i < controls.Count; i++)
            {
                controls[i].Size = recipePanelSize;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) // Menu: File: New Button
        {
            NewRecipeFile();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) // Menu: File: Open Button
        {
            OpenLoadFileWindow();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) // Menu: File: Save Button
        {
            SaveRecipeFile();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) // Menu: File: Save As Button
        {
            SaveNewRecipeFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) // Menu: File: Exit Button
        {
            Application.Exit();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e) // Menu: Tools: Options Button
        {
            OpenOptionsWindow();
        }

        private void newToolStripButton_Click(object sender, EventArgs e) // Button Strip: New Button
        {
            NewRecipeFile();
        }

        private void openToolStripButton_Click(object sender, EventArgs e) // Button Strip: Open Button
        {
            OpenLoadFileWindow();
        }

        private void saveToolStripButton_Click(object sender, EventArgs e) // Tool Strip: Save Button
        {
            SaveRecipeFile();
        }

        private void linkLabelNewWorld_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NewRecipeFile();
        }

        private void linkLabelOpenWorld_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenLoadFileWindow();
        }

        private void linkLabelRecent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel linkLabelRecent = (LinkLabel)sender;

            World.File = (string)linkLabelRecent.Tag;

            OpenRecipeFile();
        }

        private void comboBoxFilterMaterial_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterRecipes();
        }

        private void textBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            FilterRecipes();
        }

        private void buttonAddRecipe_Click(object sender, EventArgs e) // Button: Add a Recipe
        {
            AddRecipe();
        }

        private void buttonEditRecipe_Click(object sender, EventArgs e) // Button: Edit selected Recipe
        {
            buttonEditRecipe.Enabled = false;
            EditRecipe();
        }

        private void buttonDeleteRecipe_Click(object sender, EventArgs e) // Button: Delete a Recipe
        {
            buttonDeleteRecipe.Enabled = false;
            DeleteRecipe();
        }

        private void recipePanel_KeyUp(object sender, KeyEventArgs e)
        {
            Panel panel = sender as Panel;

            switch (e.KeyCode)
            {
                case Keys.Left:
                case Keys.Up:
                    //selectedRecipePanel = GetNextControl(panel, false) as Panel;
                    //selectedRecipePanel.Focus();
                    break;
                case Keys.Right:
                case Keys.Down:
                    //selectedRecipePanel = GetNextControl(panel, true) as Panel;
                    //selectedRecipePanel.Focus();
                    break;
                case Keys.Delete:
                    buttonDeleteRecipe.Enabled = false;
                    buttonEditRecipe.Enabled = false;
                    World.Recipes.Remove(selectedRecipe);
                    panel.Dispose();
                    toolStripStatusLabelSaveState.Text = "Unsaved";
                    break;
            }
        }

        private void recipePanel_GotFocus(object sender, EventArgs e)
        {
            selectedRecipePanel = sender as Panel;
            lastSelectedRecipePanel = selectedRecipePanel;
            selectedRecipe = selectedRecipePanel.Tag as Recipe;
            
            buttonDeleteRecipe.Enabled = true;
            buttonEditRecipe.Enabled = true;

            Invalidate();
            Refresh();
        }

        private void recipePanel_LostFocus(object sender, EventArgs e)
        {
            Panel panel = sender as Panel;

            selectedRecipePanel = null;
            lastSelectedRecipePanel = panel;

            panel.BorderStyle = BorderStyle.FixedSingle;
            if (Settings.GetDarkMode()) panel.BorderStyle = BorderStyle.None;

            if (!buttonDeleteRecipe.Focused) buttonDeleteRecipe.Enabled = false;
            if (!buttonEditRecipe.Focused) buttonEditRecipe.Enabled = false;
            
            Invalidate();
            Refresh();
        }
            
        private void recipePanel_Click(object sender, EventArgs e)
        {
            (sender as Panel).Focus();
        }

        private void recipePanel_Paint(object sender, PaintEventArgs p) // give selected recipePanel a blue border
        {
            Panel panel = sender as Panel;

            if (panel == selectedRecipePanel)
            {
                panel.BorderStyle = BorderStyle.None;

                ControlPaint.DrawBorder(p.Graphics, panel.ClientRectangle,
                    Color.DarkBlue, 2, ButtonBorderStyle.Solid,
                    Color.DarkBlue, 2, ButtonBorderStyle.Solid,
                    Color.DarkBlue, 2, ButtonBorderStyle.Solid,
                    Color.DarkBlue, 2, ButtonBorderStyle.Solid);

                if (Settings.GetDarkMode())
                {
                    ControlPaint.DrawBorder(p.Graphics, panel.ClientRectangle,
                    Color.FromArgb(9, 71, 113), 2, ButtonBorderStyle.Solid,
                    Color.FromArgb(9, 71, 113), 2, ButtonBorderStyle.Solid,
                    Color.FromArgb(9, 71, 113), 2, ButtonBorderStyle.Solid,
                    Color.FromArgb(9, 71, 113), 2, ButtonBorderStyle.Solid);
                }
            }
        }

        private void groupBoxFilterRecipes_Paint(object sender, PaintEventArgs p) // alter for dark mode
        {
            if (Settings.GetDarkMode())
            {
                GroupBox groupBox = (GroupBox) sender;
                Pen pen = new Pen(groupBox.ForeColor, 1);
                Point pointStartText = new Point(5, 7);
                Point pointEndText = new Point(83, 7);
                Point pointTopLeft = new Point(0, 7);
                Point pointBottomLeft = new Point(0, groupBoxFilterRecipes.ClientRectangle.Height - 1);
                Point pointTopRight = new Point(groupBoxFilterRecipes.ClientRectangle.Width - 1, 7);
                Point pointBottomRight = new Point(groupBoxFilterRecipes.ClientRectangle.Width - 1, groupBoxFilterRecipes.ClientRectangle.Height - 1);

                p.Graphics.Clear(groupBox.BackColor); // Destroys everything: text, border

                p.Graphics.DrawLine(pen, pointStartText, pointTopLeft);
                p.Graphics.DrawLine(pen, pointTopLeft, pointBottomLeft);
                p.Graphics.DrawLine(pen, pointBottomLeft, pointBottomRight);
                p.Graphics.DrawLine(pen, pointBottomRight, pointTopRight);
                p.Graphics.DrawLine(pen, pointTopRight, pointEndText);

                p.Graphics.DrawString(groupBox.Text, groupBox.Font, new SolidBrush(Color.FromArgb(150, 150, 150)), 7, 0);
            }
        }

        private void contextMenuStripRecipePanel_Opening(object sender, CancelEventArgs e) // right click on recipe panel
        {
            ContextMenuStrip contextMenuStrip = sender as ContextMenuStrip;
            Panel recipePanel = contextMenuStrip.SourceControl as Panel;
            recipePanel.Focus();
        }

        private void toolStripMenuItemDeleteRecipe_Click(object sender, EventArgs e)
        {
            DeleteRecipe();
        }

        private void toolStripMenuItemEditRecipe_Click(object sender, EventArgs e)
        {
            EditRecipe();
        }

        private void toolStripMenuItemViewRecipeNotes_Click(object sender, EventArgs e) // Not implemented
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            PromptForSave();
        }

        private class ToolStripDarkRenderer : ToolStripProfessionalRenderer
        {
            public ToolStripDarkRenderer() : base(new DarkColorTable()) { }
        }

        private class DarkColorTable : ProfessionalColorTable
        {
            public override Color MenuItemSelectedGradientBegin { // Hover over top element
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color MenuItemSelectedGradientEnd { // Hover over top element
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color MenuItemBorder { // Border of top elements
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color MenuItemPressedGradientBegin { // Background of pressed top element
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color MenuItemPressedGradientEnd { // Background of pressed top element
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color ToolStripDropDownBackground { // Background of expanded menu
                get { return Color.FromArgb(37, 37, 37); }
            }

            public override Color MenuBorder { // Border of expanded menu
                get { return Color.FromArgb(37, 37, 37); }
            }

            public override Color ToolStripGradientBegin { // This seemingly does nothing
                get { return Color.Transparent; }
            }

            public override Color ToolStripGradientMiddle { // But this is evident (top left corner of tool srip)
                get { return Color.Transparent; }
            }

            public override Color ToolStripGradientEnd { // And this (bottom left corner of tool strip)
                get { return Color.Transparent; }
            }

            public override Color ToolStripBorder { // Bottom border of the tool strip
                get { return Color.Transparent; }
            }

            public override Color MenuItemSelected { // Hover over element in expanded menu
                get { return Color.FromArgb(9, 71, 113); }
            }

            public override Color SeparatorLight { // Separator in expanded menu
                get { return Color.FromArgb(97, 97, 97); }
            }

            public override Color SeparatorDark { // Separator in expanded menu
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color GripLight { // 
                get { return Color.FromArgb(97, 97, 97); }
            }

            public override Color GripDark { // 
                get { return Color.FromArgb(80, 80, 80); }
            }

            public override Color MenuStripGradientBegin { // 
                get { return Color.Red; }
            }

            public override Color MenuStripGradientEnd { // 
                get { return Color.Red; }
            }

            public override Color RaftingContainerGradientBegin { // 
                get { return Color.Red; }
            }

            public override Color RaftingContainerGradientEnd { // 
                get { return Color.Red; }
            }

            public override Color StatusStripGradientBegin { // 
                get { return Color.Red; }
            }

            public override Color StatusStripGradientEnd { // 
                get { return Color.Red; }
            }

            public override Color ToolStripContentPanelGradientBegin { // 
                get { return Color.Red; }
            }

            public override Color ToolStripContentPanelGradientEnd { // 
                get { return Color.Red; }
            }   

            public override Color ToolStripPanelGradientBegin { // 
                get { return Color.Red; }
            }

            public override Color ToolStripPanelGradientEnd { // 
                get { return Color.Red; }
            }
        }
    }
}