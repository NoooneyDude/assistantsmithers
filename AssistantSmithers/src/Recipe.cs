﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssistantSmithers
{

    public class ItemType
    {
        public string Name { get; set; }

        public static ItemType Ingot = new ItemType { Name = "Ingot" };
        public static ItemType General = new ItemType { Name = "General" };
        public static ItemType Tool = new ItemType { Name = "Tool" };
        public static ItemType Weapon = new ItemType { Name = "Weapon" };
        public static ItemType Armour = new ItemType { Name = "Armour" };
        public static ItemType Block = new ItemType { Name = "Block" };

        public static List<ItemType> GetItemTypes()
        {
            return new List<ItemType>
            {
                Ingot, General, Tool, Weapon, Armour, Block
            };
        }
    }

    public class Metal
    {
        public string Name { get; set; }
        public int Tier { get; set; }
        public bool Alloy { get; set; }
        public List<Metal> AlloyIngredients { get; set; }

        public static Metal Bismuth = new Metal { Name = "Bismuth", Tier = 0 };
        public static Metal Tin = new Metal { Name = "Tin", Tier = 0 };
        public static Metal Zinc = new Metal { Name = "Zinc", Tier = 0 };
        public static Metal Copper = new Metal { Name = "Copper", Tier = 1 };
        public static Metal Bronze = new Metal { Name = "Bronze", Tier = 2, Alloy = true, AlloyIngredients = new List<Metal> { Copper, Tin } };
        public static Metal BismuthBronze = new Metal { Name = "Bismuth Bronze", Tier = 2 };
        public static Metal BlackBronze = new Metal { Name = "Black Bronze", Tier = 2 };
        public static Metal Brass = new Metal { Name = "Brass", Tier = 2 };
        public static Metal Lead = new Metal { Name = "Lead", Tier = 2 };
        public static Metal Gold = new Metal { Name = "Gold", Tier = 2 };
        public static Metal RoseGold = new Metal { Name = "Rose Gold", Tier = 2 };
        public static Metal Silver = new Metal { Name = "Silver", Tier = 2 };
        public static Metal SterlingSilver = new Metal { Name = "Sterling Silver", Tier = 2 };
        public static Metal Platinum = new Metal { Name = "Platinum", Tier = 3 };
        public static Metal WroughtIron = new Metal { Name = "Wrought Iron", Tier = 3 };
        public static Metal Nickel = new Metal { Name = "Nickel", Tier = 4 };
        public static Metal PigIron = new Metal { Name = "Pig Iron", Tier = 4 };
        public static Metal Steel = new Metal { Name = "Steel", Tier = 4 };
        public static Metal BlackSteel = new Metal { Name = "Black Steel", Tier = 5 };
        public static Metal BlueSteel = new Metal { Name = "Blue Steel", Tier = 6 };
        public static Metal RedSteel = new Metal { Name = "Red Steel", Tier = 6 };
        public static Metal UnknownMetal = new Metal { Name = "Unknown Metal" };
        public static Metal IronBloom = new Metal { Name = "Iron Bloom" };

        public static List<Metal> GetMetals()
        {
            return new List<Metal>
            {
                Bismuth, Tin, Zinc, Copper, Bronze, BismuthBronze, BlackBronze, Brass,
                Lead, Gold, RoseGold, Silver, SterlingSilver, Platinum, WroughtIron,
                Nickel, PigIron, Steel, BlackSteel, BlueSteel, RedSteel, IronBloom
            };
        }
    }

    public class AnvilOperation
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public static AnvilOperation Any = new AnvilOperation { Name = "Any", Type = null };
        public static AnvilOperation Hit = new AnvilOperation { Name = "Hit", Type = "subtractive" };
        public static AnvilOperation LightHit = new AnvilOperation { Name = "Light Hit", Type = "subtractive" };
        public static AnvilOperation MediumHit = new AnvilOperation { Name = "Medium Hit", Type = "subtractive" };
        public static AnvilOperation HeavyHit = new AnvilOperation { Name = "Heavy Hit", Type = "subtractive" };
        public static AnvilOperation Draw = new AnvilOperation { Name = "Draw", Type = "subtractive" };
        public static AnvilOperation Punch = new AnvilOperation { Name = "Punch", Type = "additive" };
        public static AnvilOperation Bend = new AnvilOperation { Name = "Bend", Type = "additive" };
        public static AnvilOperation Upset = new AnvilOperation { Name = "Upset", Type = "additive" };
        public static AnvilOperation Shrink = new AnvilOperation { Name = "Shrink", Type = "additive" };

        public static List<AnvilOperation> GetAnvilOperations()
        {
            return new List<AnvilOperation>
            {
                Any, Hit, LightHit, MediumHit, HeavyHit, Draw, Punch, Bend, Upset, Shrink
            };
        }

        public override string ToString()
        {
            return "[AnvilOperation] Name: " + Name + ". Type: " + Type + ".";
        }
    }

    [Serializable]
    public class Recipe
    {
        public ItemType ItemType { get; set; }
        public string Name { get; set; }
        public Metal Material { get; set; }
        public AnvilOperation RuleOne { get; set; }
        public AnvilOperation RuleTwo { get; set; }
        public AnvilOperation RuleThree { get; set; }
        public List<KeyValuePair<AnvilOperation, int>> Steps { get; set; }
        public string Notes { get; set; }

        public static List<Recipe> StoredRecipes = new List<Recipe>();

        public override string ToString()
        {
            string str = "Recipe: " + Material.Name + " " + Name + "\n" +
                "Item Type: " + ItemType.Name + "\n" +
                "Rule One: " + RuleOne.Name + "\n" +
                "Rule Two: " + RuleTwo.Name + "\n" +
                "Rule Three: " + RuleThree.Name + "\n";

            foreach (KeyValuePair<AnvilOperation, int> step in Steps)
            {
                str += "Step: " + step.Value + " " + step.Key.Name + "\n";
            }

            str += "Notes: " + Notes;

            return str;
        }
    }
}
