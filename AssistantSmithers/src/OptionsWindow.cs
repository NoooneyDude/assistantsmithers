﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistantSmithers
{
    public partial class OptionsWindow : Form
    {
        public OptionsWindow()
        {
            InitializeComponent();
            if (Settings.GetDarkMode()) checkBoxDarkMode.Checked = true;
        }

        private void checkBoxDarkMode_CheckedChanged(object sender, EventArgs e)
        {
            Settings.SetDarkMode(checkBoxDarkMode.Checked);
        }
    }
}
