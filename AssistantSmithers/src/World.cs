﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssistantSmithers
{
    static class World
    {
        public static string Name;
        public static string File;
        public static List<Recipe> Recipes = new List<Recipe>();

        public static void Reset()
        {
            Name = null;
            File = null;
            Recipes = new List<Recipe>();
        }
    }
}
