﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

/* XML Example:
 * <Recipes worldName="Brave New World">
 *     <General>
 *         <Recipe material="Wrought Iron" name="Sheet">
 *             <Rule>Shrink</Rule>
 *             <Rule>Bend</Rule>
 *             <Rule>Hit</Rule>
 *             <Step count=6>Shrink</Step>
 *             <Step count=1>Punch</Step>
 *             <Notes></Notes>
 *         </Sheet>
 *     <Tools>
 *     </Tools>
 * </Recipes>
 */

namespace AssistantSmithers
{
    static class XMLOperations
    {
        private static XmlDocument recipeXML;
        private static XmlNode ingotRecipes;
        private static XmlNode generalRecipes;
        private static XmlNode toolRecipes;
        private static XmlNode weaponRecipes;
        private static XmlNode armourRecipes;
        private static XmlNode blockRecipes;

        public static void GenerateXmlDocument()
        {
            recipeXML = new XmlDocument();

            XmlElement recipes = recipeXML.CreateElement("Recipes");
            XmlAttribute worldName = recipeXML.CreateAttribute("worldName");
            worldName.Value = World.Name;
            recipes.Attributes.Append(worldName);
            recipeXML.AppendChild(recipes);

            ingotRecipes = recipeXML.CreateElement("Ingots");
            generalRecipes = recipeXML.CreateElement("General");
            toolRecipes = recipeXML.CreateElement("Tools");
            weaponRecipes = recipeXML.CreateElement("Weapons");
            armourRecipes = recipeXML.CreateElement("Armour");
            blockRecipes = recipeXML.CreateElement("Blocks");

            recipes.AppendChild(ingotRecipes);
            recipes.AppendChild(generalRecipes);
            recipes.AppendChild(toolRecipes);
            recipes.AppendChild(weaponRecipes);
            recipes.AppendChild(armourRecipes);
            recipes.AppendChild(blockRecipes);
        }

        public static void SaveRecipe(Recipe recipe)
        {
            if (string.IsNullOrEmpty(recipe.Name)) return;

            XmlElement newRecipe = recipeXML.CreateElement("Recipe");
            XmlAttribute recipeMaterial = recipeXML.CreateAttribute("material");
            XmlAttribute recipeName = recipeXML.CreateAttribute("name");
            recipeMaterial.Value = recipe.Material.Name;
            recipeName.Value = recipe.Name;
            newRecipe.Attributes.Append(recipeMaterial);
            newRecipe.Attributes.Append(recipeName);

            XmlElement ruleOne = recipeXML.CreateElement("Rule");
            XmlElement ruleTwo = recipeXML.CreateElement("Rule");
            XmlElement ruleThree = recipeXML.CreateElement("Rule");

            ruleOne.InnerText = recipe.RuleOne.Name;
            ruleTwo.InnerText = recipe.RuleTwo.Name;
            ruleThree.InnerText = recipe.RuleThree.Name;

            newRecipe.AppendChild(ruleOne);
            newRecipe.AppendChild(ruleTwo);
            newRecipe.AppendChild(ruleThree);

            foreach (KeyValuePair<AnvilOperation, int> step in recipe.Steps)
            {
                XmlElement newStep = recipeXML.CreateElement("Step");
                XmlAttribute stepCount = recipeXML.CreateAttribute("count");

                newStep.InnerText = step.Key.Name;
                stepCount.Value = step.Value.ToString();

                newStep.Attributes.Append(stepCount);
                newRecipe.AppendChild(newStep);
            }

            XmlElement notes = recipeXML.CreateElement("Notes");
            notes.InnerText = recipe.Notes;
            newRecipe.AppendChild(notes);

            if (recipe.ItemType == ItemType.Ingot) ingotRecipes.AppendChild(newRecipe);
            else if (recipe.ItemType == ItemType.General) generalRecipes.AppendChild(newRecipe);
            else if (recipe.ItemType == ItemType.Tool) toolRecipes.AppendChild(newRecipe);
            else if (recipe.ItemType == ItemType.Weapon) weaponRecipes.AppendChild(newRecipe);
            else if (recipe.ItemType == ItemType.Armour) armourRecipes.AppendChild(newRecipe);
            else if (recipe.ItemType == ItemType.Block) blockRecipes.AppendChild(newRecipe);
            else generalRecipes.AppendChild(newRecipe);
        }

        public static void SaveAllRecipes()
        {
            World.Recipes.Sort((x, y) => x.Name.CompareTo(y.Name));

            foreach (Recipe recipe in World.Recipes)
            {
                SaveRecipe(recipe);
            }
        }

        private static void LoadRecipe(XmlNode XMLRecipe)
        {
            XmlNode material = XMLRecipe.Attributes.GetNamedItem("material");
            XmlNode name = XMLRecipe.Attributes.GetNamedItem("name");
            XmlNode ruleOne = XMLRecipe.FirstChild;
            XmlNode ruleTwo = ruleOne.NextSibling;
            XmlNode ruleThree = ruleTwo.NextSibling;

            Recipe recipe = new Recipe();

            string itemType;
            if (XMLRecipe.ParentNode.Name == "Ingots") itemType = "Ingot";
            else if (XMLRecipe.ParentNode.Name == "Tools") itemType = "Tool";
            else if (XMLRecipe.ParentNode.Name == "Weapons") itemType = "Weapon";
            else if (XMLRecipe.ParentNode.Name == "Blocks") itemType = "Block";
            else itemType = XMLRecipe.ParentNode.Name;

            recipe.ItemType = ItemType.GetItemTypes().Find(type => type.Name == itemType);
            recipe.Name = name.Value;
            recipe.Material = Metal.GetMetals().Find(metal => metal.Name == material.Value);
            recipe.RuleOne = AnvilOperation.GetAnvilOperations().Find(op => op.Name == ruleOne.InnerText);
            recipe.RuleTwo = AnvilOperation.GetAnvilOperations().Find(op => op.Name == ruleTwo.InnerText);
            recipe.RuleThree = AnvilOperation.GetAnvilOperations().Find(op => op.Name == ruleThree.InnerText);
            recipe.Steps = new List<KeyValuePair<AnvilOperation, int>>();

            XmlNode currentNode = ruleThree;
            while (currentNode.NextSibling != null)
            {
                currentNode = currentNode.NextSibling;
                if (currentNode.Name == "Notes") break;

                AnvilOperation operation = AnvilOperation.GetAnvilOperations().Find(op => op.Name == currentNode.InnerText);
                int stepCount = int.Parse(currentNode.Attributes.GetNamedItem("count").Value);

                recipe.Steps.Add(new KeyValuePair<AnvilOperation, int>(operation, stepCount));
            }

            recipe.Notes = currentNode.InnerText;

            World.Recipes.Add(recipe);
        }

        private static void LoadAllRecipesFromCategory(XmlNode category)
        {
            foreach (XmlNode XMLRecipe in category.ChildNodes)
            {
                LoadRecipe(XMLRecipe);
            }
        }

        private static void LoadAllRecipes()
        {
            foreach (XmlNode category in recipeXML.FirstChild.ChildNodes)
            {
                LoadAllRecipesFromCategory(category);
            }
        }

        public static void SaveXMLFile()
        {
            recipeXML.Save(World.File);
        }

        public static string LoadXMLFile()
        {
            recipeXML = new XmlDocument();

            if (!System.IO.File.Exists(World.File))
            {
                return "File doesn't exist.";
            }        

            try
            {
                recipeXML.Load(World.File);
            }
            catch (XmlException e)
            {
                Console.WriteLine("File not valid XML: " + e.Message);
                return "File not valid XML.";
            }

            if (!ValidateXML())
            {
                return "File not valid recipe file.";
            }

            World.Name = recipeXML.FirstChild.Attributes.GetNamedItem("worldName").Value;
            LoadAllRecipes();

            return "File loaded.";
        }

        private static bool ValidateXML()
        {
            string xsdResource = Properties.Resources.XMLRecipeFileSchema;
            System.IO.StringReader xsdReader = new System.IO.StringReader(xsdResource);
            recipeXML.Schemas.Add(XmlSchema.Read(xsdReader, null));

            bool validity = true;
            string failMessage = "File validation failed:";
            recipeXML.Validate((sender, e) => {
                validity = false;
                failMessage += Environment.NewLine + "\t" + e.Message;
            });

            if (!validity)
            {
                Console.WriteLine(failMessage);
            }

            return validity;
        }

        public static void SerializeRecipeObjects()
        {
            foreach (Recipe recipe in World.Recipes)
            {
                new XmlSerializer(typeof(Recipe)).Serialize(Console.Out, recipe);
            }
        }
    }
}
