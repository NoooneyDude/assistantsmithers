using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssistantSmithers
{
    public partial class RecipeDialog : Form
    {
        private int stepCount = 1;

        public RecipeDialog() // creating a new recipe
        {
            Initialise();
        }

        public RecipeDialog(Recipe originalRecipe) // editing an existing recipe
        {
            Initialise();

            comboBoxRecipeName.Text = originalRecipe.Name;
            comboBoxRecipeCategory.SelectedItem = originalRecipe.ItemType;
            comboBoxRecipeMaterial.SelectedItem = originalRecipe.Material;
            comboBoxRecipeRule1.SelectedItem = originalRecipe.RuleOne;
            comboBoxRecipeRule2.SelectedItem = originalRecipe.RuleTwo;
            comboBoxRecipeRule3.SelectedItem = originalRecipe.RuleThree;
            comboBoxRecipeStep1.SelectedItem = originalRecipe.Steps[0].Key;
            numericUpDownRecipeStep1.Value = originalRecipe.Steps[0].Value;

            for (int i = 1; i < originalRecipe.Steps.Count; i++)
            {
                AddStep(); // only the first step already exists

                ComboBox stepComboBox = groupBoxSteps.Controls["comboBoxRecipeStep" + (i + 1)] as ComboBox;
                NumericUpDown stepNumericUpDown = groupBoxSteps.Controls["numericUpDownRecipeStep" + (i + 1)] as NumericUpDown;
                stepComboBox.DataSource = AnvilOperation.GetAnvilOperations();  // WHY do we need to redefine datasource here?

                stepComboBox.SelectedItem = originalRecipe.Steps[i].Key;
                stepNumericUpDown.Value = originalRecipe.Steps[i].Value;
            }

            richTextBoxNotes.Text = originalRecipe.Notes;
        }

        private void Initialise()
        {
            InitializeComponent();

            //comboBoxRecipeName.DataSource = Item.GetItems();
            //comboBoxRecipeName.DisplayMember = "Name";

            comboBoxRecipeCategory.DataSource = ItemType.GetItemTypes();
            comboBoxRecipeCategory.DisplayMember = "Name";

            comboBoxRecipeMaterial.DataSource = Metal.GetMetals();
            comboBoxRecipeMaterial.DisplayMember = "Name";

            comboBoxRecipeRule1.DataSource = AnvilOperation.GetAnvilOperations();
            comboBoxRecipeRule1.DisplayMember = "Name";

            comboBoxRecipeRule2.DataSource = AnvilOperation.GetAnvilOperations();
            comboBoxRecipeRule2.DisplayMember = "Name";

            comboBoxRecipeRule3.DataSource = AnvilOperation.GetAnvilOperations();
            comboBoxRecipeRule3.DisplayMember = "Name";

            comboBoxRecipeStep1.DataSource = AnvilOperation.GetAnvilOperations();
            comboBoxRecipeStep1.DisplayMember = "Name";

            numericUpDownRecipeStep1.Value = 1;
        }

        public Recipe getRecipe()
        {
            List<KeyValuePair<AnvilOperation, int>> stepList = new List<KeyValuePair<AnvilOperation, int>>();

            for (int i = 1; i <= stepCount; i++)
            {
                ComboBox stepComboBox = groupBoxSteps.Controls["comboBoxRecipeStep" + i] as ComboBox;
                NumericUpDown stepNumericUpDown = groupBoxSteps.Controls["numericUpDownRecipeStep" + i] as NumericUpDown;

                AnvilOperation step = stepComboBox.SelectedItem as AnvilOperation;
                int stepCount = (int) stepNumericUpDown.Value;

                stepList.Add(new KeyValuePair<AnvilOperation, int>(step, stepCount));
            }

            return new Recipe
            {
                Name = comboBoxRecipeName.Text,
                ItemType = (ItemType) comboBoxRecipeCategory.SelectedValue,
                Material = (Metal) comboBoxRecipeMaterial.SelectedValue,
                RuleOne = (AnvilOperation) comboBoxRecipeRule1.SelectedValue,
                RuleTwo = (AnvilOperation) comboBoxRecipeRule2.SelectedValue,
                RuleThree = (AnvilOperation) comboBoxRecipeRule3.SelectedValue,
                Steps = stepList,
                Notes = richTextBoxNotes.Text,
            };
        }

        private void AddStep()
        {
            stepCount++;

            if (stepCount > 1) buttonRemoveStep.Visible = true;
            buttonRemoveStep.Location = new Point(241, 9 + (27 * stepCount));

            Label stepLabel = new Label
            {
                Name = "labelRecipeStep" + stepCount,
                Location = new Point(10, 13 + (27 * stepCount)),
                AutoSize = true,
                Text = "Step " + stepCount,
            };

            ComboBox stepComboBox = new ComboBox
            {
                Name = "comboBoxRecipeStep" + stepCount,
                Location = new Point(55, 10 + (27 * stepCount)),
                DataSource = AnvilOperation.GetAnvilOperations(),
                DisplayMember = "Name",
            };

            NumericUpDown stepNumericUpDown = new NumericUpDown
            {
                Name = "numericUpDownRecipeStep" + stepCount,
                Location = new Point(185, 10 + (27 * stepCount)),
                Size = new Size(50, 20),
                Value = 1,
            };

            groupBoxSteps.Controls.Add(stepLabel);
            groupBoxSteps.Controls.Add(stepComboBox);
            groupBoxSteps.Controls.Add(stepNumericUpDown);
        }

        private void buttonAddStep_Click(object sender, EventArgs e) // add step
        {
            AddStep();
        }

        private void buttonRemoveStep_Click(object sender, EventArgs e) // remove step
        {
            Control stepLabel = groupBoxSteps.Controls["labelRecipeStep" + stepCount];
            Control stepComboBox = groupBoxSteps.Controls["comboBoxRecipeStep" + stepCount];
            Control stepNumericUpDown = groupBoxSteps.Controls["numericUpDownRecipeStep" + stepCount];

            stepLabel.Dispose();
            stepComboBox.Dispose();
            stepNumericUpDown.Dispose();

            stepCount--;

            if (stepCount < 2) buttonRemoveStep.Visible = false;
            else buttonRemoveStep.Location = new Point(241, 9 + (27 * stepCount));
        }

        private void comboBoxRecipeName_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(comboBoxRecipeName.Text))
            {
                e.Cancel = true;
                comboBoxRecipeName.Focus();
                errorProvider.SetError(comboBoxRecipeName, "Name should not be left blank.");
            }
            else
            {
                e.Cancel = false;
                errorProvider.SetError(comboBoxRecipeName, "");
            }
        }
    }
}
