﻿namespace AssistantSmithers
{
    partial class RecipeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecipeDialog));
            this.labelRecipeName = new System.Windows.Forms.Label();
            this.labelRecipeCategory = new System.Windows.Forms.Label();
            this.comboBoxRecipeName = new System.Windows.Forms.ComboBox();
            this.comboBoxRecipeCategory = new System.Windows.Forms.ComboBox();
            this.comboBoxRecipeMaterial = new System.Windows.Forms.ComboBox();
            this.comboBoxRecipeRule1 = new System.Windows.Forms.ComboBox();
            this.comboBoxRecipeRule2 = new System.Windows.Forms.ComboBox();
            this.labelRecipeMaterial = new System.Windows.Forms.Label();
            this.labelRecipeRule1 = new System.Windows.Forms.Label();
            this.labelRecipeRule2 = new System.Windows.Forms.Label();
            this.comboBoxRecipeRule3 = new System.Windows.Forms.ComboBox();
            this.labelRecipeRule3 = new System.Windows.Forms.Label();
            this.buttonAddStep = new System.Windows.Forms.Button();
            this.groupBoxSteps = new System.Windows.Forms.GroupBox();
            this.buttonRemoveStep = new System.Windows.Forms.Button();
            this.numericUpDownRecipeStep1 = new System.Windows.Forms.NumericUpDown();
            this.labelRecipeStep1 = new System.Windows.Forms.Label();
            this.labelStepCount = new System.Windows.Forms.Label();
            this.comboBoxRecipeStep1 = new System.Windows.Forms.ComboBox();
            this.labelStepMethod = new System.Windows.Forms.Label();
            this.richTextBoxNotes = new System.Windows.Forms.RichTextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBoxNotes = new System.Windows.Forms.GroupBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBoxSteps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecipeStep1)).BeginInit();
            this.groupBoxNotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRecipeName
            // 
            this.labelRecipeName.AutoSize = true;
            this.labelRecipeName.Location = new System.Drawing.Point(13, 13);
            this.labelRecipeName.Name = "labelRecipeName";
            this.labelRecipeName.Size = new System.Drawing.Size(35, 13);
            this.labelRecipeName.TabIndex = 0;
            this.labelRecipeName.Text = "Name";
            // 
            // labelRecipeCategory
            // 
            this.labelRecipeCategory.AutoSize = true;
            this.labelRecipeCategory.Location = new System.Drawing.Point(13, 41);
            this.labelRecipeCategory.Name = "labelRecipeCategory";
            this.labelRecipeCategory.Size = new System.Drawing.Size(49, 13);
            this.labelRecipeCategory.TabIndex = 1;
            this.labelRecipeCategory.Text = "Category";
            // 
            // comboBoxRecipeName
            // 
            this.comboBoxRecipeName.FormattingEnabled = true;
            this.comboBoxRecipeName.Location = new System.Drawing.Point(79, 10);
            this.comboBoxRecipeName.Name = "comboBoxRecipeName";
            this.comboBoxRecipeName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeName.TabIndex = 2;
            this.comboBoxRecipeName.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxRecipeName_Validating);
            // 
            // comboBoxRecipeCategory
            // 
            this.comboBoxRecipeCategory.FormattingEnabled = true;
            this.comboBoxRecipeCategory.Location = new System.Drawing.Point(79, 38);
            this.comboBoxRecipeCategory.Name = "comboBoxRecipeCategory";
            this.comboBoxRecipeCategory.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeCategory.TabIndex = 3;
            // 
            // comboBoxRecipeMaterial
            // 
            this.comboBoxRecipeMaterial.FormattingEnabled = true;
            this.comboBoxRecipeMaterial.Location = new System.Drawing.Point(79, 65);
            this.comboBoxRecipeMaterial.Name = "comboBoxRecipeMaterial";
            this.comboBoxRecipeMaterial.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeMaterial.TabIndex = 4;
            // 
            // comboBoxRecipeRule1
            // 
            this.comboBoxRecipeRule1.FormattingEnabled = true;
            this.comboBoxRecipeRule1.Location = new System.Drawing.Point(79, 92);
            this.comboBoxRecipeRule1.Name = "comboBoxRecipeRule1";
            this.comboBoxRecipeRule1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeRule1.TabIndex = 5;
            // 
            // comboBoxRecipeRule2
            // 
            this.comboBoxRecipeRule2.FormattingEnabled = true;
            this.comboBoxRecipeRule2.Location = new System.Drawing.Point(79, 119);
            this.comboBoxRecipeRule2.Name = "comboBoxRecipeRule2";
            this.comboBoxRecipeRule2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeRule2.TabIndex = 6;
            // 
            // labelRecipeMaterial
            // 
            this.labelRecipeMaterial.AutoSize = true;
            this.labelRecipeMaterial.Location = new System.Drawing.Point(13, 68);
            this.labelRecipeMaterial.Name = "labelRecipeMaterial";
            this.labelRecipeMaterial.Size = new System.Drawing.Size(33, 13);
            this.labelRecipeMaterial.TabIndex = 7;
            this.labelRecipeMaterial.Text = "Metal";
            // 
            // labelRecipeRule1
            // 
            this.labelRecipeRule1.AutoSize = true;
            this.labelRecipeRule1.Location = new System.Drawing.Point(13, 95);
            this.labelRecipeRule1.Name = "labelRecipeRule1";
            this.labelRecipeRule1.Size = new System.Drawing.Size(52, 13);
            this.labelRecipeRule1.TabIndex = 8;
            this.labelRecipeRule1.Text = "Rule One";
            // 
            // labelRecipeRule2
            // 
            this.labelRecipeRule2.AutoSize = true;
            this.labelRecipeRule2.Location = new System.Drawing.Point(13, 122);
            this.labelRecipeRule2.Name = "labelRecipeRule2";
            this.labelRecipeRule2.Size = new System.Drawing.Size(53, 13);
            this.labelRecipeRule2.TabIndex = 9;
            this.labelRecipeRule2.Text = "Rule Two";
            // 
            // comboBoxRecipeRule3
            // 
            this.comboBoxRecipeRule3.FormattingEnabled = true;
            this.comboBoxRecipeRule3.Location = new System.Drawing.Point(79, 147);
            this.comboBoxRecipeRule3.Name = "comboBoxRecipeRule3";
            this.comboBoxRecipeRule3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeRule3.TabIndex = 10;
            // 
            // labelRecipeRule3
            // 
            this.labelRecipeRule3.AutoSize = true;
            this.labelRecipeRule3.Location = new System.Drawing.Point(13, 150);
            this.labelRecipeRule3.Name = "labelRecipeRule3";
            this.labelRecipeRule3.Size = new System.Drawing.Size(60, 13);
            this.labelRecipeRule3.TabIndex = 11;
            this.labelRecipeRule3.Text = "Rule Three";
            // 
            // buttonAddStep
            // 
            this.buttonAddStep.Location = new System.Drawing.Point(241, 36);
            this.buttonAddStep.Name = "buttonAddStep";
            this.buttonAddStep.Size = new System.Drawing.Size(60, 23);
            this.buttonAddStep.TabIndex = 12;
            this.buttonAddStep.Text = "Add Step";
            this.buttonAddStep.UseVisualStyleBackColor = true;
            this.buttonAddStep.Click += new System.EventHandler(this.buttonAddStep_Click);
            // 
            // groupBoxSteps
            // 
            this.groupBoxSteps.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxSteps.Controls.Add(this.buttonRemoveStep);
            this.groupBoxSteps.Controls.Add(this.numericUpDownRecipeStep1);
            this.groupBoxSteps.Controls.Add(this.labelRecipeStep1);
            this.groupBoxSteps.Controls.Add(this.labelStepCount);
            this.groupBoxSteps.Controls.Add(this.comboBoxRecipeStep1);
            this.groupBoxSteps.Controls.Add(this.labelStepMethod);
            this.groupBoxSteps.Controls.Add(this.buttonAddStep);
            this.groupBoxSteps.Location = new System.Drawing.Point(206, 10);
            this.groupBoxSteps.Name = "groupBoxSteps";
            this.groupBoxSteps.Size = new System.Drawing.Size(310, 307);
            this.groupBoxSteps.TabIndex = 13;
            this.groupBoxSteps.TabStop = false;
            this.groupBoxSteps.Text = "Reproduction Steps";
            // 
            // buttonRemoveStep
            // 
            this.buttonRemoveStep.Location = new System.Drawing.Point(241, 64);
            this.buttonRemoveStep.Name = "buttonRemoveStep";
            this.buttonRemoveStep.Size = new System.Drawing.Size(60, 23);
            this.buttonRemoveStep.TabIndex = 19;
            this.buttonRemoveStep.Text = "Remove";
            this.buttonRemoveStep.UseVisualStyleBackColor = true;
            this.buttonRemoveStep.Visible = false;
            this.buttonRemoveStep.Click += new System.EventHandler(this.buttonRemoveStep_Click);
            // 
            // numericUpDownRecipeStep1
            // 
            this.numericUpDownRecipeStep1.Location = new System.Drawing.Point(185, 38);
            this.numericUpDownRecipeStep1.Name = "numericUpDownRecipeStep1";
            this.numericUpDownRecipeStep1.Size = new System.Drawing.Size(50, 20);
            this.numericUpDownRecipeStep1.TabIndex = 18;
            // 
            // labelRecipeStep1
            // 
            this.labelRecipeStep1.AutoSize = true;
            this.labelRecipeStep1.Location = new System.Drawing.Point(10, 40);
            this.labelRecipeStep1.Name = "labelRecipeStep1";
            this.labelRecipeStep1.Size = new System.Drawing.Size(38, 13);
            this.labelRecipeStep1.TabIndex = 17;
            this.labelRecipeStep1.Text = "Step 1";
            // 
            // labelStepCount
            // 
            this.labelStepCount.AutoSize = true;
            this.labelStepCount.Location = new System.Drawing.Point(182, 21);
            this.labelStepCount.Name = "labelStepCount";
            this.labelStepCount.Size = new System.Drawing.Size(35, 13);
            this.labelStepCount.TabIndex = 16;
            this.labelStepCount.Text = "Count";
            // 
            // comboBoxRecipeStep1
            // 
            this.comboBoxRecipeStep1.FormattingEnabled = true;
            this.comboBoxRecipeStep1.Location = new System.Drawing.Point(55, 37);
            this.comboBoxRecipeStep1.Name = "comboBoxRecipeStep1";
            this.comboBoxRecipeStep1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRecipeStep1.TabIndex = 15;
            // 
            // labelStepMethod
            // 
            this.labelStepMethod.AutoSize = true;
            this.labelStepMethod.Location = new System.Drawing.Point(106, 21);
            this.labelStepMethod.Name = "labelStepMethod";
            this.labelStepMethod.Size = new System.Drawing.Size(69, 13);
            this.labelStepMethod.TabIndex = 14;
            this.labelStepMethod.Text = "Anvil Method";
            // 
            // richTextBoxNotes
            // 
            this.richTextBoxNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxNotes.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxNotes.Name = "richTextBoxNotes";
            this.richTextBoxNotes.Size = new System.Drawing.Size(294, 291);
            this.richTextBoxNotes.TabIndex = 14;
            this.richTextBoxNotes.Text = "";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(747, 326);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 15;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(666, 326);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // groupBoxNotes
            // 
            this.groupBoxNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxNotes.Controls.Add(this.richTextBoxNotes);
            this.groupBoxNotes.Location = new System.Drawing.Point(522, 10);
            this.groupBoxNotes.Name = "groupBoxNotes";
            this.groupBoxNotes.Size = new System.Drawing.Size(300, 310);
            this.groupBoxNotes.TabIndex = 17;
            this.groupBoxNotes.TabStop = false;
            this.groupBoxNotes.Text = "Notes";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // RecipeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(834, 361);
            this.Controls.Add(this.groupBoxNotes);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBoxSteps);
            this.Controls.Add(this.labelRecipeRule3);
            this.Controls.Add(this.comboBoxRecipeRule3);
            this.Controls.Add(this.labelRecipeRule2);
            this.Controls.Add(this.labelRecipeRule1);
            this.Controls.Add(this.labelRecipeMaterial);
            this.Controls.Add(this.comboBoxRecipeRule2);
            this.Controls.Add(this.comboBoxRecipeRule1);
            this.Controls.Add(this.comboBoxRecipeMaterial);
            this.Controls.Add(this.comboBoxRecipeCategory);
            this.Controls.Add(this.comboBoxRecipeName);
            this.Controls.Add(this.labelRecipeCategory);
            this.Controls.Add(this.labelRecipeName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 230);
            this.Name = "RecipeDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AssistantSmithers: Recipe";
            this.groupBoxSteps.ResumeLayout(false);
            this.groupBoxSteps.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRecipeStep1)).EndInit();
            this.groupBoxNotes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRecipeName;
        private System.Windows.Forms.Label labelRecipeCategory;
        private System.Windows.Forms.ComboBox comboBoxRecipeName;
        private System.Windows.Forms.ComboBox comboBoxRecipeCategory;
        private System.Windows.Forms.ComboBox comboBoxRecipeMaterial;
        private System.Windows.Forms.ComboBox comboBoxRecipeRule1;
        private System.Windows.Forms.ComboBox comboBoxRecipeRule2;
        private System.Windows.Forms.Label labelRecipeMaterial;
        private System.Windows.Forms.Label labelRecipeRule1;
        private System.Windows.Forms.Label labelRecipeRule2;
        private System.Windows.Forms.ComboBox comboBoxRecipeRule3;
        private System.Windows.Forms.Label labelRecipeRule3;
        private System.Windows.Forms.Button buttonAddStep;
        private System.Windows.Forms.GroupBox groupBoxSteps;
        private System.Windows.Forms.Label labelRecipeStep1;
        private System.Windows.Forms.Label labelStepCount;
        private System.Windows.Forms.ComboBox comboBoxRecipeStep1;
        private System.Windows.Forms.Label labelStepMethod;
        private System.Windows.Forms.NumericUpDown numericUpDownRecipeStep1;
        private System.Windows.Forms.RichTextBox richTextBoxNotes;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.GroupBox groupBoxNotes;
        private System.Windows.Forms.Button buttonRemoveStep;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}