# AssistantSmithers: A TFC Assistant

A simple program (with GUI) that stores anvil recipes for the Minecraft mod TerraFirmaCraft.

Essentially a glorified notebook. As the recipes are specific to a given world, the program works with just one recipe set from a given world at any time.

## Author

* **NoooneyDude** - *Start to finish* - [NoooneyDude](https://gitlab.com/NoooneyDude)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the GNU v3 License - see the [LICENSE.md](LICENSE.md) file for details